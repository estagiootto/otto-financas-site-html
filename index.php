<!doctype html>
<html class="no-js" lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Otto Finanças</title>
  
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta property="og:title" content="Otto Finanças" />
  <meta property="og:type" content="website" />
  <meta property="og:description" content="Faça mais por suas finanças com o Otto!" />
  <meta property="og:image" content="https://ottofinancas.com.br/assets/images/otto-simbolo.png" />
  <meta name="facebook-domain-verification" content="8zjqqkvchww398dsqrst2vgkh3s182" />

  <link href="favicon.svg" rel="icon">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
  <link rel="text/plain" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/styles.css">
  <style>
    .cookieConsentContainer {
      z-index: 999;
      width: 350px;
      min-height: 20px;
      box-sizing: border-box;
      padding: 30px 30px 30px 30px;
      background: #232323;
      overflow: hidden;
      position: fixed;
      bottom: 80px;
      right: 30px;
      display: none
    }
  
    .cookieConsentContainer .cookieTitle a {
      font-family: OpenSans, arial, sans-serif;
      color: #fff;
      font-size: 22px;
      line-height: 20px;
      display: block
    }
  
    .cookieConsentContainer .cookieDesc p {
      margin: 0;
      padding: 0;
      font-family: OpenSans, arial, sans-serif;
      color: #fff;
      font-size: 13px;
      line-height: 20px;
      display: block;
      margin-top: 10px
    }
  
    .cookieConsentContainer .cookieDesc a {
      font-family: OpenSans, arial, sans-serif;
      color: #fff;
      text-decoration: underline
    }
  
    .cookieConsentContainer .cookieButton a {
      display: inline-block;
      font-family: OpenSans, arial, sans-serif;
      color: #fff;
      font-size: 14px;
      font-weight: 700;
      margin-top: 14px;
      background: #000;
      box-sizing: border-box;
      padding: 15px 24px;
      text-align: center;
      transition: background .3s
    }
  
    .cookieConsentContainer .cookieButton a:hover {
      cursor: pointer;
      background: #3e9b67
    }
  
    @media (max-width:980px) {
      .cookieConsentContainer {
        bottom: 0 !important;
        left: 0 !important;
        width: 100% !important
      }
    }
  </style>

  <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id=%27+i+dl;f.parentNode.insertBefore(j,f)';
  })(window,document,'script','dataLayer','GTM-WLT42V7');</script>
  <!-- End Google Tag Manager -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-KMGKFWPXL4"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-KMGKFWPXL4');
  </script>

</head>

<body id="top">
  <!--[if lte IE 9]>
    <p class="browserupgrade">Você está usando um navegador <strong>desatualizado</strong>. Por favor <a href="https://browsehappy.com/">atualize seu navegador</a> para mehorar sua experiência e segurança.</p>
  <![endif]-->

  <!-- Meta Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '3362593753975378');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3362593753975378&ev=PageView&noscript=1" />
  </noscript>
  <!-- End Meta Pixel Code -->

  <div id="app">

    <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-gradient">
      <div class="container is-max-widescreen">
        <a class="navbar-brand smoothScroll" href="#pageTop">
          <img class="original-brand" src="assets/images/logo-otto.svg" width="120">
          <img class="white-brand" src="assets/images/logo-otto-br.svg" width="120">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link smoothScroll" href="planos">Planos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link smoothScroll" href="#baixar">Baixar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link smoothScroll" href="#seventh">FAQ</a>
            </li>
            <li class="nav-item">
              <a class="nav-link smoothScroll" href="https://blog.ottofinancas.com.br" target="_blank">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link smoothScroll" href="https://empresas.ottofinancas.com.br" target="_blank">Para Empresas</a>
            </li>
          </ul>
          <div class="columns">
            <button class="loginButton is-hidden-mobile" onclick="window.open('https://app.ottofinancas.com.br/login')">Login Otto Web</button>
          </div>
        </div>
      </div>
    </nav>

    <section id="first" class="hero">

      <div class="block-one container is-max-widescreen">
        <div class="columns">
          <div class="column">
            <h1>Seu orçamento bem planejado. Você rumo a uma vida financeira próspera.</h1>

            <p>Com o Otto você entende melhor sua situação financeira, recebe orientações para otimizar seu orçamento e controla receitas e gastos em um só lugar.</p>
              
            <button onclick="window.open('https://app.ottofinancas.com.br/login')">Comece agora!</button>
          </div>

          <div class="column">
            <div class="hero-image">
              <img id="smartphone-hero" class="img-conform-block-one" src="assets/images/hero-image/smartphone.svg">
              <img id="wallet" src="assets/images/hero-image/wallet.svg">
              <img id="coin1" src="assets/images/hero-image/coin1.svg">
              <img id="coin2" src="assets/images/hero-image/coin2.svg">
              <img id="coin3" src="assets/images/hero-image/coin3.svg">
            </div>
          </div>

          <div class="is-hidden">
            <img class="img-conform-block-one" src="assets/images/blockOne2.png">
          </div>
        </div>
      </div>

    </section>

    <section id="second" class="full-height">
      <div class="container is-max-widescreen block-two">
        <div class="columns">
          <div class="column is-hidden-mobile">
            <div class="smartdemo">
              <!-- <img class="img-conform-block-two" src="assets/images/smartChat.svg"> -->
              <img id="chatUser" src="assets/images/chatUser.svg" alt="Resposta do usuário" class="lazy">
              <img id="chatOtto" src="assets/images/chatOtto.svg" alt="Pergunta do Otto" class="lazy">
              <img id="chatPhone" src="assets/images/chatPhone.svg" alt="Smartphone com Otto aberto" class="lazy">
            </div>
          </div>
          <div class="column">
            <h1>Tenha consciência do seu<br> momento de vida, e realize <br>seus objetivos</h1>
            <ul>
              <li class="row row-mobile">
                <div class="list-circle">
                  <img src="assets/images/categorias.svg">
                </div>
                <div>
                  <h2>Diagnóstico financeiro</h2>
                  <p>Tenha um raio-x da sua saúde financeira e acompanhe a evolução do seu perfil.</p>
                </div>
              </li>
              <li class="row row-mobile">
                <div class="list-circle">
                  <img src="assets/images/bank-conn.svg">
                </div>
                <div>
                  <h2>Integração bancária</h2>
                  <p>Conecte suas contas e cartões ao Otto e nunca mais esqueça de registrar uma despesa.</p>
                </div>
              </li>
              <li class="row row-mobile">
                <div class="list-circle">
                  <img src="assets/images/mao-segurando-um-saco-de-dinheiro-com-dolares.svg">
                </div>
                <div>
                  <h2>Planeje seu orçamento</h2>
                  <p>Defina limites para categorias de despesas e o Otto te avisa quando estiver prestes a atingi-los.</p>
                </div>
              </li>

              <div class="column is-hidden-desktop">
                <div class="smartdemo">
                  <img class="img-conform-block-two" src="assets/images/chat-w-otto.svg">
                </div>
              </div>

            </ul>
          </div>
        </div>
      </div>
    </section>

    <section id="third" class="columns is-centered">
      <div class="container is-max-widescreen">
        <div class="block-three row row-mobile">
          <h1>Acompanhe seus resultados,<br> defina metas e planeje as<br> melhores estratégias</h1>
          <div class="vertical-line">
            <p>Planejando as finanças fica mais fácil acompanhar o ciclo do seu dinheiro de forma detalhada, e a
              traçar a melhor estratégia para alcançar suas realizações pessoais. Vamos começar?</p>
          </div>
        </div>

        <button class="startNow" onclick="window.open('https://app.ottofinancas.com.br')">Tô dentro!</button>
      </div>
    </section>

    <section id="fourth">
      <div class="block-four">
        <div class="container is-max-widescreen">

          <div class="block-four-header">
            <h1>Sua jornada de vida mais<br> saudável, com menos imprevistos e<br> mais realizações.</h1>
            <hr class="is-hidden-desktop">
          </div>
          <div class="columns">
            <div class="fourth_left-column">
              <ul>
                <li style="margin-bottom: 45px;">
                  <div class="columns block-four-item-mb">
                    <div class="list-circle ">
                      <img src="assets/images/transferir.svg">
                    </div>
                    <div>
                      <h2>Notificações</h2>
                      <p>Nunca mais esqueça de pagar suas contas.</p>
                    </div>
                  </div>
                </li>
                <li style="margin-bottom: 45px;">
                  <div class="columns block-four-item-mb">
                    <div class="list-circle ">
                      <img src="assets/images/suporte-24-horas.svg">
                    </div>
                    <div>
                      <h2>Suporte amigo</h2>
                      <p>Se precisar, estarei disposto em ajudar.</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="columns block-four-item-mb">
                    <div class="list-circle ">
                      <img src="assets/images/dados-seguros.svg">
                    </div>
                    <div>
                      <h2>Dados seguros</h2>
                      <p>Adequado as normas previstas na Lei Geral de Proteção de Dados (LGPD). Nada será compartilhado,
                        apenas você terá acesso.</p>
                    </div>
                  </div>
                </li>
              </ul>

            </div>

            <div class="phones-group">
              <!-- <img class="img-conform-block-four" src="assets/images/blockFour1.png"> -->
              <img id="backPhone" src="assets/images/tela_movimento.png" alt="Tela Movimentações" class="abs-pos back-phone lazy">
              <img id="frontPhone" src="assets/images/tela_card_objetivo.png" alt="Tela inicial com card de Objetivos" class="abs-pos front-phone lazy">
            </div>
          </div>

        </div>
      </div>
    </section>

    <section id="baixar" class="bg-orange py-5 mb-5">
      <div class="container">
        <div class="block-four-footer">
          <div class="center">
            <div class="columns is-centered center" style="margin-bottom: 0;">
              <img class="column is-one-fifth image is-128x128 otto-face is-hidden-mobile"
                src="assets/images/otto-heart.svg">
              <img class="column is-one-fifth image is-128x128 otto-face is-hidden-desktop"
                src="assets/images/otto-chat.svg">
              <h3 class="column">+ de 300mil</h3>
            </div>

            <div class="columns is-centered">
              <h4 class="column">pessoas simplificaram sua vida financeira</h4>
            </div>

            <button onclick="window.open('https://play.google.com/store/apps/details?id=com.ottoapp')"
              class="store-button">
              <div class="left">
                <img class="google-icon" src="assets/images/google.svg">
                Baixe na Google Play
              </div>
            </button>
            <button class="store-button" onclick="window.open('https://apps.apple.com/br/app/otto-finan%C3%A7as/id1207689926')">
              <div class="left">
                <img class="apple-icon" src="assets/images/apple.svg">
                Baixe na App Store
              </div>
            </button>
          </div>
        </div>
      </div>
    </section>

    <section id="sixth" class="full-height">
      <div class="block-six container is-max-widescreen">

        <h1>Experiência de quem usa o Otto</h1>

        <div class="is-hidden-mobile">

          <div class="columns testimony-box testimony-box-mb">
            <img class="column testimony-box-circle" src="assets/images/user1.png">
            <div class="column" style="padding-left: 0px !important; padding-bottom: 0px !important;">
              <div class="columns" style="margin-bottom: 15px !important;">
                <div class="row stars">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                </div>
                <h2>Vida financeira estável!</h2>
              </div>
              <p>Ótimo, recomendo a todos que desejam ter uma vida financeira estável, desde que seja regrada e
                disciplinada seguindo todo o objetivo do aplicativo, a equipe de desenvolvimento esta de parabéns</p>
              <h2 style="font-style: italic;">- Everson Junior</h2>
            </div>
          </div>

          <div class="columns testimony-box testimony-box-mb">
            <img class="column testimony-box-circle" src="assets/images/user2.png">
            <div class="column" style="padding-left: 0px !important; padding-bottom: 0px !important;">
              <div class="columns" style="margin-bottom: 15px !important;">
                <div class="row stars">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                </div>
                <h2>Esperanças renovadas!</h2>
              </div>
              <p>Gente, a melhor coisa que já me aconteceu! Esperanças renovadas de acertar a vida financeira.</p>
              <h2 style="font-style: italic;">- Pri Goes</h2>
            </div>
          </div>

          <div class="columns testimony-box testimony-box-mb">
            <img class="column testimony-box-circle" src="assets/images/user3.png">
            <div class="column" style="padding-left: 0px !important; padding-bottom: 0px !important;">
              <div class="columns" style="margin-bottom: 15px !important;">
                <div class="row stars">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                  <img src="assets/images/estrela.svg">
                </div>
                <h2>Despesas controladas!</h2>
              </div>
              <p>Simplesmente o melhor! Realmente ajuda mesmo. Cálculos super certos. Me ajuda demais. Controlo 100%
                minhas despesas!</p>
              <h2 style="font-style: italic;">- Cássia Massilon</h2>
            </div>
          </div>

        </div>

        <div class="gallery js-flickity is-hidden-desktop"
          data-flickity-options='{ "prevNextButtons": false, "wrapAround": true }'>

          <div class="gallery-cell">
            <div class="testimony-box testimony-box-mb is-one-quarter">
              <div class="testimony-box-circle">
                <img src="assets/images/user1.png">
              </div>
              <h2>Everson Junior</h2>
              <h2>Vida financeira estável!</h2>
              <p>Ótimo, recomendo a todos que desejam ter uma vida financeira estável, desde que seja regrada e
                disciplinada seguindo todo o objetivo do aplicativo, a equipe de desenvolvimento esta de parabéns</p>
              <div class="row stars">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
              </div>
            </div>
            <p>arraste para o lado ></p>
          </div>

          <div class="gallery-cell">
            <div class="testimony-box testimony-box-mb is-one-quarter">
              <div class="testimony-box-circle">
                <img src="assets/images/user2.png">
              </div>
              <h2>Pri Goes</h2>
              <h2>Esperanças renovadas!</h2>
              <p>Gente, a melhor coisa que já me aconteceu! Esperanças renovadas de acertar a vida financeira.</p>
              <div class="row stars">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
              </div>
            </div>
            <p>arraste para o lado ></p>
          </div>

          <div class="gallery-cell">
            <div class="testimony-box testimony-box-mb is-one-quarter">
              <div class="testimony-box-circle">
                <img src="assets/images/user3.png">
              </div>
              <h2>Cássia Massilon</h2>
              <h2>Despesas controladas!</h2>
              <p>Simplesmente o melhor! Realmente ajuda mesmo. Cálculos super certos. Me ajuda demais. Controlo 100%
                minhas despesas!</p>
              <div class="row stars">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
                <img src="assets/images/estrela.svg">
              </div>
            </div>
            <p>arraste para o lado ></p>
          </div>
        </div>
    </section>

    <section id="seventh">
      <div class="block-seven container is-max-widescreen center">
        <div id="faq">
          <h1>Perguntas frequentes</h1>
          <div id="accordionFaq">
          </div>
        </div>
      </div>
    </section>

    <section id="eighth" class="full-height">
      <div class="container is-max-widescreen block-eight">

        <h1>Finanças: descubra e aprenda novas<br> possibilidades todos os dias!</h1>

        <div class="columns is-variable is-4 is-centered">

          <div class="is-loading" v-if="isLoading"></div>

          <div class="article column is-one-third" v-for="(post, index) in posts" :key="index">

            <div class="img" :style="'background-image: url('+post._embedded['wp:featuredmedia']['0'].source_url+');'"></div>

            <h2>{{ post.title.rendered }}</h2>

            <p>{{ post.excerpt.rendered }}</p>

            <a :href="post.guid.rendered" target="_blank">Leia mais ></a>
          </div>
        </div>
      </div>
    </section>

    <section id="nineth" class="section form instagram-section">
      <div class="container is-max-widescreen instagram-section block-nine">
        
        <!-- 
        <div class="instagram-box">
          <h1>Nosso instagram: <br> 
            <a href="https://www.instagram.com/ottoassistente/?hl=pt-br" target="_blank" rel="noopener noreferrer">@ottoassistente</a>
          </h1>

          <div id="instagram-photos" class="columns is-centered is-hidden-mobile">
          </div>

          <div id="instagram-photos-mobile" class="js-flickity is-hidden-desktop"
            data-flickity-options='{ "prevNextButtons": false, "wrapAround": true }'>
            <div class="instagram-cell center">
            </div>
            <div class="instagram-cell center">
            </div>
            <div class="instagram-cell center">
            </div>
            <div class="instagram-cell center">
            </div>
            <div class="instagram-cell center">
            </div>
          </div>
        </div>
         -->

        <form id="contact-form" action="scripts/mail.php" method="post">
          <h1>Contato</h1>

          <div>
            <label for="nome">Nome completo *</label>
            <input type="text" name="name" required>
          </div>
          <div>
            <label for="email-contact">Email *</label>
            <input type="text" name="email-contact" required>
          </div>
          <div style="margin-bottom: 30px;">
            <label for="message">Escreva sua mensagem</label>
            <textarea name="message" cols="30" rows="5" required></textarea>
          </div>
          <div class="center">
            <div class="contact-button contact-button-mobile">
              <button type="submit">Enviar dados</button>
            </div>
          </div>
        </form>

        <div class="otto-balloon is-hidden-mobile">
          <img src="assets/images/otto-balloon.svg" alt="Otto" class="lazy">
        </div>

      </div>
    </section>

    <footer class="footer">
      <div class="to-top">
        <a class="smoothScroll" href="#top">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
            <path id="Icon_awesome-arrow-alt-circle-up" data-name="Icon awesome-arrow-alt-circle-up" d="M.563,20.563a20,20,0,1,1,20,20A20,20,0,0,1,.563,20.563Zm23.548,9.355V20.563h5.718a.968.968,0,0,0,.685-1.653L21.248,9.692a.959.959,0,0,0-1.363,0l-9.274,9.218a.968.968,0,0,0,.685,1.653h5.718v9.355a.971.971,0,0,0,.968.968h5.161A.971.971,0,0,0,24.111,29.917Z" transform="translate(-0.563 -0.563)" fill="#fff"/>
          </svg>
        </a>
      </div>

      <div class="container in-footer">

        <div class="row align-items-center">
          <div class="col-md-6 col-sm-12 text-right">
            <div class="footer-logo">
              <img src="assets/images/logo-otto-br.svg">
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <ul>
              <li>
                <a href="https://empresas.ottofinancas.com.br/" target="_blank">Otto para Empresas</a>
              </li>
              <li>
                <a href="https://blog.ottofinancas.com.br/" target="_blank">Blog do Otto</a>
              </li>
              <li>
                <a href="https://app.ottofinancas.com.br/login" target="_blank">Cadastre-se</a>
              </li>
              <li>
                <a href="termos-de-uso/index.html" target="_blank">Termos de Uso</a>
              </li>
              <li>
                <a href="politica-de-privacidade/index.html" target="_blank">Política de Privacidade</a>
              </li>
            </ul>
          </div>
        </div>

        <div class="social-media text-center my-4">
          <a href="https://pt-br.facebook.com/ottoassistente/" target="_blank" rel="noopener noreferrer"><i
              class="fab fa-facebook fa-2x"></i></a>
          <a href="https://linkedin.com/company/otto-financas" target="_blank" rel="noopener noreferrer"><i
              class="fab fa-linkedin fa-2x"></i></a>
          <a href="https://instagram.com/ottofinancas/" target="_blank" rel="noopener noreferrer"><i
              class="fab fa-instagram fa-2x"></i></a>
        </div>

        <p class="text-center">Otto Sistemas Ltda</p>

      </div>
    </footer>

  </div>

  <!-- WA BUTTON -->
  <div id="gb-widget-8430"
  style="bottom: 14px; right: 16px; opacity: 1; transition: opacity 0.5s ease 0s; box-sizing: border-box; direction: ltr; text-align: right; position: fixed !important; z-index: 16000160 !important;">
    <div class="q8c6tt-2 eiGYSu">
      <style>
        .bt-wa { flex-shrink: 0;width: 50px;height: 50px;order: 2;padding: 5px;box-sizing: border-box;border-radius: 50%;cursor: pointer;overflow: hidden;box-shadow: rgb(0 0 0 / 40%) 2px 2px 6px;transition: all 0.5s ease 0s;position: relative;z-index: 200;display: block;border: 0px;background-color: rgb(77, 194, 71) !important; }
        .bt-wa:hover {
          box-shadow: rgb(0 0 0 / 70%) 2px 2px 11px;
        }
      </style>
      <a size="50" href="https://wa.me/5575988016074/?text=Olá! Gostaria de ajuda a respeito do Otto." target="_blank" color="#4dc247" class="bt-wa">
        <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          style="width: 100%; height: 100%; fill: rgb(255, 255, 255); stroke: none;">
          <path
            d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z">
          </path>
        </svg>
      </a>
    </div>
  </div>

  <script>
    function faqFunction(arr) {
      var out = '';
      var i;
      for (i = 0; i < arr.length; i++) {
        out += '<div class="card"><div class="card-header" id="heading' + i + '"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse' + i + '" aria-expanded="false" aria-controls="collapse' + i + '">' + '<div class="card-item"><p>' + arr[i].p +'</p></button></div><div id="collapse' + i + '" class="collapse" aria-labelledby="heading' + i + '" data-parent="#accordionFaq"><div class="card-body">' + arr[i].r + '</div></div></div>';
      }
      document.getElementById("accordionFaq").innerHTML = out;
    }
  </script>
  <script async defer src="scripts/faq.js"></script>

  <!-- SCRIPTS
  <script>
    var purecookieTitle = "Cookies",
      purecookieDesc =
      "Nosso site utiliza cookies que coletam algumas informações que visam melhorar sua experiência de uso. Para continuar, você deve aceitar nossa ",
      purecookieLink =
      '<a href="https://ottofinancas.com.br/politica-de-privacidade/" target="_blank">Política de Cookies.</a>',
      purecookieButton = "Entendi";

    function pureFadeIn(e, o) {
      var i = document.getElementById(e);
      i.style.opacity = 0, i.style.display = o || "block",
        function e() {
          var o = parseFloat(i.style.opacity);
          (o += .02) > 1 || (i.style.opacity = o, requestAnimationFrame(e))
        }()
    }

    function pureFadeOut(e) {
      var o = document.getElementById(e);
      o.style.opacity = 1,
        function e() {
          (o.style.opacity -= .02) < 0 ? o.style.display = "none" : requestAnimationFrame(e)
        }()
    }

    function setCookie(e, o, i) {
      var t = "";
      if (i) {
        var n = new Date;
        n.setTime(n.getTime() + 24 * i * 60 * 60 * 1e3), t = "; expires=" + n.toUTCString()
      }
      document.cookie = e + "=" + (o || "") + t + "; path=/"
    }

    function getCookie(e) {
      for (var o = e + "=", i = document.cookie.split(";"), t = 0; t < i.length; t++) {
        for (var n = i[t];
          " " == n.charAt(0);) n = n.substring(1, n.length);
        if (0 == n.indexOf(o)) return n.substring(o.length, n.length)
      }
      return null
    }

    function eraseCookie(e) {
      document.cookie = e + "=; Max-Age=-99999999;"
    }

    function cookieConsent() {
      getCookie("purecookieDismiss") || (document.body.innerHTML +=
        '<div class="cookieConsentContainer" id="cookieConsentContainer"><div class="cookieTitle"><a>' +
        purecookieTitle + '</a></div><div class="cookieDesc"><p>' + purecookieDesc + " " + purecookieLink +
        '</p></div><div class="cookieButton"><a onClick="purecookieDismiss();">' + purecookieButton +
        "</a></div></div>", pureFadeIn("cookieConsentContainer"))
    }

    function purecookieDismiss() {
      setCookie("purecookieDismiss", "1", 7), pureFadeOut("cookieConsentContainer");
      document.location.reload(true);
    }
    window.onload = function () {
      cookieConsent();
    };
  </script>
  -->

  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <!-- <script async defer src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script async defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
  </script>
  <script>
    window.jQuery || document.write('<script src="scripts/vendor/jquery-3.3.1.min.js"><\/script>')
  </script>
  <script async defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
  </script>

  <!-- Libs Necessárias! -->
  <script src="assets/lazyLoading.js"></script>
  <script src="scripts//smoothScrolling.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script src="scripts/main.js"></script>
  <script>
    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar.sticky-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  </script>

  <!-- Script Principal do Feed Instagram-->
  <!-- <script src="scripts/scripts.js"></script> -->

  <!-- Sweetalert -->
  <?php
  if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
  ?>
    <script>
      var status = '<?php echo $_SESSION['status']; ?>';
      var statusCode = '<?php echo $_SESSION['status_code']; ?>';

      Swal.fire(
        status,
        '',
        statusCode
      );
    </script>
  <?php
    unset($_SESSION['status']);
  }
  ?>
</body>

</html>
