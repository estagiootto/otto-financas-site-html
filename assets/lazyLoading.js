document.addEventListener("DOMContentLoaded", function() {
    var lazyImages = document.querySelectorAll("img.lazy");    
    var lazyloadThrottleTimeout;
    
    function lazyload () {
      if(lazyloadThrottleTimeout) {
        clearTimeout(lazyloadThrottleTimeout);
      }    
      
      lazyloadThrottleTimeout = setTimeout(function() {
          var screenBottom = window.innerHeight;
          lazyImages.forEach(function(item) {
              var elem = item.getBoundingClientRect();
              if(elem.top <= screenBottom) {
                item.classList.add('entered');
              }
          });
          
          if(lazyImages.length == 0) { 
            document.removeEventListener("scroll", lazyload);
            window.removeEventListener("resize", lazyload);
            window.removeEventListener("orientationChange", lazyload);
          }
      }, 20);
    }
    
    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
});