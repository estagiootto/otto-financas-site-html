let ax = axios.create({
  baseURL: 'https://blog.ottofinancas.com.br/wp-json/wp/v2/'
});

var app = new Vue({
  el: '#app',

  data: {
    isLoading: false,
    posts: [],
  },

  mounted() {
    this.isLoading = true
    ax.get('posts?_embed&per_page=3&order=desc&orderby=id')
      .then(response => {
        this.posts = response.data;
        this.isLoading = false
      })
  }
})