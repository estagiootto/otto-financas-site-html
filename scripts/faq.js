faqFunction([{
  p: "Como o Otto sabe quanto eu posso gastar por dia?",
  r: "Ele pega o quanto você já recebeu no mês (receita real), subtrai o valor total do que você ainda tem para pagar (despesa projetada) e divide pela quantidade de dias restantes no mês. Assim, você sabe quanto é seguro gastar por dia até o final do mês."
}, {
  p: "Registrei uma despesa no cartão de crédito, mas ela desapareceu!",
  r: "Com o Otto você gerencia seus cartões de crédito como se estivesse utilizando o aplicativo do próprio cartão. Sendo assim, todos os gastos em um determinado cartão são lançados em uma lista separada (a fatura) e, a depender da data da sua compra, ela será incluída na fatura do mês atual ou na do mês seguinte, coincidindo com o que é apresentado na fatura que seu cartão lhe entrega."
}, {
  p: "O Otto se conecta aos meus bancos e operadoras de cartão?",
  r: "Sim! Por enquanto provemos acesso a alguns dos principais bancos e operadoras de cartão, como Banco do Brasil, Caixa, Itaú, Santander e Nubank. Em breve teremos também acesso ao Bradesco e outras instituições."
}, {
  p: "Por que não consigo cadastrar um cartão de débito?",
  r: "O cartão de débito, como o nome indica, é um meio prático e rápido de efetuar um débito na sua conta bancária. Então, pra todos os efeitos, ao utilizar um cartão de débito, você está sacando um valor da sua conta. Sendo assim, registrar um gasto e indicar que pagou usando a sua conta bancária tem o mesmo efeito de indicar o pagamento usando um cartão de débito. Em outras palavras, sua conta bancária já funciona como o seu cartão de débito."
}, {
  p: "O que são despesas essenciais e não-essenciais?",
  r: "As despesas essenciais são todas aquelas que você não pode abrir mão, como as que são categorizadas como alimentação, moradia, educação, saúde e transporte. Embora dificilmente seja possível eliminá-las do seu orçamento, existem meios de reduzir o custo com elas, aliviando assim, o orçamento."
}, {
  p: "Como posso criar minhas próprias categorias?",
  r: "Acesse a tela de configurações do Otto pelo menu lateral e escolha a opção Categorias. Use o botão + para criar sua categoria personalizada."
}, {
  p: "Onde está a opção de Orçamento Conjunto?",
  r: "Esta nova versão do Otto ainda não possui a função de compartilhamento entre usuários (orçamento conjunto), mas está em nossos planos termos ela de volta em breve."
}, {
  p: "O atendimento PREMIUM é 24 horas?",
  r: "Por enquanto não. Nosso atendimento ocorre de segunda a sexta, das 8h às 18h."
}, ]);