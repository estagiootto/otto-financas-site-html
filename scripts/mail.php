<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../phpmailer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
$nome		= $_POST["name"];
$email		= $_POST["email-contact"];
$mensagem	= $_POST["message"];
$conteudo = "Nome: $nome \n\n E-mail: $email \n\n Mensagem: $mensagem \n";

try {
    //Server settings
    $mail->isSMTP();
    $mail->Host       = 'mail.ottofinancas.com.br';
    $mail->SMTPAuth   = true;
    $mail->Username   = 'contato@ottofinancas.com.br';
    $mail->Password   = 'zXWjvwpnDC6E1V#&';
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port       = 587;
    $mail->CharSet = 'utf-8';
    //Recipients
    $mail->setFrom('contato@ottofinancas.com.br', $nome);
    $mail->addAddress('contato@ottofinancas.com.br');

    // Content
    $mail->isHTML(true);
    $mail->Subject = 'Email de contato - Otto Finanças - Site';
    $mail->Body    = $conteudo;
    $mail->AltBody = $conteudo;

    $response = $mail->send();

    session_start();

    if ($response) {
        $_SESSION['status'] = "Dados enviados com sucesso!";
        $_SESSION['status_code'] = "success";
    } else {
        $_SESSION['status'] = "Não foi possível registrar contato";
        $_SESSION['status_code'] = "error";
    }

    header('location: https://ottofinancas.com.br/#contact-form');
} catch (Exception $e) {
    $_SESSION['status'] = "Mensagem não enviada. Mailer Error: {$mail->ErrorInfo}";
    $_SESSION['status_code'] = "error";

    echo "Mensagem não enviada. Mailer Error: {$mail->ErrorInfo}";
}