const instagramRegExp = new RegExp(/<script type="text\/javascript">window\._sharedData = (.*);<\/script>/)

const fetchInstagramPhotos = async (accountUrl) => {
  const response = await axios.get(accountUrl)
  const json = JSON.parse(response.data.match(instagramRegExp)[1])
  const edges = json.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.splice(0, 5) // Altera a quantidade de post a ser exibido
  const photos = edges.map(({ node }) => {
    return {
      url: `https://www.instagram.com/p/${node.shortcode}/`, // Monta o link para redirecionamento
      thumbnailUrl: node.thumbnail_src,
      displayUrl: node.display_url,
      caption: node.edge_media_to_caption.edges[0].node.text
    }
  })
  return photos
}

(async () => {
  try {
    const photos = await fetchInstagramPhotos('https://www.instagram.com/ottofinancas/') 
    const container = document.getElementById('instagram-photos')
    const containerMobile = document.getElementsByClassName('flickity-slider')[1]
    photos.forEach(function (el, i) {
      const div = document.createElement('div')
      const divMobile = document.createElement('div')
      const a = document.createElement('a')
      const aMobile = document.createElement('a')
      const img = document.createElement('img')
      const imgMobile = document.createElement('img')

      div.classList.add('column')
      div.classList.add('instagram-cell')

      divMobile.classList.add('instagram-cell')
      if (i === 0) {
        divMobile.classList.add('is-selected')
        divMobile.setAttribute('style', 'position: absolute; left: 0%;')
      }
      if (i === 1) {
        divMobile.setAttribute('style', 'position: absolute; left: 55.56%;')
        divMobile.setAttribute('aria-hidden', 'true')
      }
      if (i === 2) {
        divMobile.setAttribute('style', 'position: absolute; left: 111.11%;')
        divMobile.setAttribute('aria-hidden', 'true')
      }
      if (i === 3) {
        divMobile.setAttribute('style', 'position: absolute; left: 166.67%;')
        divMobile.setAttribute('aria-hidden', 'true')
      }
      if (i === 4) {
        divMobile.setAttribute('style', 'position: absolute; left: -55.56%;')
        divMobile.setAttribute('aria-hidden', 'true')
      }
      divMobile.classList.add('center')

      a.setAttribute('href', el.url)
      a.setAttribute('target', '_blank')
      a.setAttribute('rel', 'noopener noreferrer')

      aMobile.setAttribute('href', el.url)
      aMobile.setAttribute('target', '_blank')
      aMobile.setAttribute('rel', 'noopener noreferrer')

      img.setAttribute('src', el.thumbnailUrl)
      img.setAttribute('alt', el.caption)
      img.classList.add('instagram-img')

      imgMobile.setAttribute('src', el.thumbnailUrl)
      imgMobile.setAttribute('alt', el.caption)
      
      a.appendChild(img)
      aMobile.appendChild(imgMobile)
      div.appendChild(a)
      divMobile.appendChild(aMobile)
      container.appendChild(div)
      containerMobile.appendChild(divMobile)
    })
  } catch (e) {
    console.error('Falha ao buscar fotos do Instagram', e)
  }
})()